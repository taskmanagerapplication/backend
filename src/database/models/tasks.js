const mongoose = require('mongoose')
const CONSTANTS = require('../../utils/constants')

const tasksSchema = mongoose.Schema({
    name: String,
    status: String
})

module.exports = mongoose.model(CONSTANTS.DATABASE.COLLECTIONS.TASKS, tasksSchema)