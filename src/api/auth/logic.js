const database = require('../users/database')
const bcrypt = require('bcrypt')
const { StatusCodes } = require('http-status-codes')
const Helpers = require('../../utils/helpers')
const { STATUS } = require('../../utils/constants')

module.exports={
    login: async user =>{
        
        const foundUser = await database.getByEmail(user.email)
        
        if(foundUser === null)
            return Helpers.handleResponse(STATUS.INCORECT_EMAIL, StatusCodes.FORBIDDEN)
        else if(bcrypt.compareSync(user.password, foundUser.password))
            return {foundUser}
        else return Helpers.handleResponse(STATUS.INCORRECT_PASS, StatusCodes.FORBIDDEN) 
        
    }
}
