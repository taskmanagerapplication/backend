const express = require('express')
const router = express.Router()
const logic = require('./logic')

router.route('/')
    .post((req, res) => {
        console.log(req.body.user)
        logic.login(req.body.user).then(resp => { 
            res.send(resp)
            
        })
    })

module.exports = router