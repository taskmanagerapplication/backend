const mongoose = require('mongoose')
const CONSTANTS = require('../../utils/constants')

const managersSchema = mongoose.Schema({
    name: String,
    surname: String,
    email: String,
    password: String,
    role: String,
    phone: String
})

module.exports = mongoose.model(CONSTANTS.DATABASE.COLLECTIONS.MANAGERS, managersSchema)