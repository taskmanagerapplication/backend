const express = require('express')
const router = express.Router()
const logic = require('./logic')

router.route('/')
    .post((req, res) => {
        logic.create(req.body.task).then(task=> {
            console.log(req.body.task)
            res.send(task)
        })
    })
    .get((req, res) => {
        console.log(req.query.status)
        logic.getAllTasks(req.query.status).then(tasks => {
            res.send(tasks)
        })
    })
router.route('/:ID')
    .put((req, res) => {
        logic.update(req.params.ID, req.body.task).then(resp => {
            res.send(resp)
        })
    })

module.exports = router