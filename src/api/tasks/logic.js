const database = require('./database')

module.exports={
    create: task => database.create(task),
    getAllTasks: status => database.getAllTasks(status),
    update: (id, task) => database.update(id, task)
}