const express = require('express')
const router = express.Router()
const logic = require('./logic')

router.route('/')
    .post((req, res) => {
        logic.create(req.body.user).then(user=> {
            res.send(user)
        })
    })
    .get((req, res) => {
        logic.getAllEmployees().then(employees => {
            res.send(employees)
        })
    })

module.exports = router