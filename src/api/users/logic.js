const database = require('./database')
const bcrypt = require('bcrypt')
const {SALT_ROUNDS} = require('../../utils/constants')

module.exports={
    create: user =>{ 
        user.password = bcrypt.hashSync(user.password, SALT_ROUNDS)
        return database.create(user)
    },
    getAllEmployees: () => database.getAllEmployees()
}