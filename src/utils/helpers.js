module.exports = {
    handleResponse: (status, code) => {
        return {status: status, code: code}
    }
}