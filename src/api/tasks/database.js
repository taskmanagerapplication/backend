const tasks = require('../../database/models/tasks')

module.exports = {
    create: task => tasks.create(task),
    getAllTasks : status => tasks.find({status: status}),
    update: (id, task) => tasks.findByIdAndUpdate(id, task)
}