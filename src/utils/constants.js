module.exports = {
    PORT: 9000,
    DATABASE: {
        COLLECTIONS:{
            MANAGERS : 'managers',
            TASKS: 'tasks'
        },
        URL : 'mongodb://localhost:27017/taskmanagerdb'
    },
    SALT_ROUNDS: 10,
    STATUS: {
        INCORRECT_PASS: "Incorrect password",
        OK : "OK",
        INCORECT_EMAIL : "Incorrect email"
    },
    ORIGIN: 'http://localhost:3000'

}