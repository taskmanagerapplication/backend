const users = require('../../database/models/users')

module.exports = {
    create: user => users.create(user),
    getByEmail: email => users.findOne({email : email}).lean().exec(),
    getAllEmployees: () => users.find({role: 'employee'}).lean().exec()
}